package rytone.chroma;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;

public class StatusWorker extends Thread {
	String[] statuses;
	JDA jda;
	public StatusWorker(JDA j) {
		this.statuses = new String[]{"V1.0", "chroma.rytone.tk"};
		this.jda = j;
	}
	public void run() {
		int statusi = 0;
		try {
			while (true) {
				this.jda.getPresence().setGame(Game.of(this.statuses[statusi]));
				statusi = (statusi + 1) % statuses.length;
				Thread.sleep(20000);
			}
		} catch (InterruptedException e) {
			return;
		}
	}
}
