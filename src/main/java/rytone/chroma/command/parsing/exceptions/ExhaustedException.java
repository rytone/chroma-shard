package rytone.chroma.command.parsing.exceptions;

public class ExhaustedException extends Exception {
	public ExhaustedException(String msg) {
		super(msg);
	}
}
