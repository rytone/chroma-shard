package rytone.chroma.command.parsing.exceptions;

public class UnmatchedPrefixException extends Exception {
	public UnmatchedPrefixException(String msg) {
		super(msg);
	}
}
