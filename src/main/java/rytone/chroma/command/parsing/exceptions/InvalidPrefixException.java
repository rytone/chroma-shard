package rytone.chroma.command.parsing.exceptions;

public class InvalidPrefixException extends Exception {
	public InvalidPrefixException(String msg) {
		super(msg);
	}
}
