package rytone.chroma.command.parsing.exceptions;

public class InvalidArgumentTypeException extends Exception {
	public InvalidArgumentTypeException(String msg) {
		super(msg);
	}
}
