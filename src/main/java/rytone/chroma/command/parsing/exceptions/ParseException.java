package rytone.chroma.command.parsing.exceptions;

public class ParseException extends Exception {
	public ParseException(String msg) {
		super(msg);
	}
}
