package rytone.chroma.command.parsing;

import rytone.chroma.command.parsing.exceptions.InvalidPrefixException;
import rytone.chroma.command.parsing.exceptions.ExhaustedException;
import rytone.chroma.command.parsing.exceptions.ParseException;
import rytone.chroma.command.parsing.exceptions.UnmatchedPrefixException;

import java.util.HashMap;

public class CommandParser {
	private String toParse;
	private HashMap<String, ArgumentType> parsedArguments = new HashMap<>();

	public CommandParser(String raw) {
		this.toParse = raw;
	}

	public CommandParser prefix(String pfx) throws InvalidPrefixException, UnmatchedPrefixException {
		if (pfx.equals("")) {
			throw new InvalidPrefixException("Prefix cannot be empty");
		}

		if (toParse.startsWith(pfx)) {
			this.toParse = this.toParse.substring(pfx.length(), this.toParse.length());
		} else {
			throw new UnmatchedPrefixException("Passed command does not match the specified prefix");
		}

		return this;
	}

	public CommandParser argument(ArgumentType type, String name) throws ExhaustedException, ParseException {
		if (this.toParse.equals("")) {
			throw new ExhaustedException("Passed command string has been exhausted");
		}

		ParseResult res = type.parse(this.toParse);
		this.toParse = res.unparsed;
		this.parsedArguments.put(name, res.result);
		return this;
	}

	public String command() {
		String cmd = this.toParse.split("\\s+")[0];
		this.toParse = this.toParse.substring(cmd.length(), this.toParse.length()).trim();
		return cmd;
	}

	public ArgumentType getArgument(String name) {
		return this.parsedArguments.get(name);
	}
}
