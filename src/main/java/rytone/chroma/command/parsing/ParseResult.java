package rytone.chroma.command.parsing;

public class ParseResult {
	public final ArgumentType result;
	public final String unparsed;

	public ParseResult(ArgumentType res, String up) {
		this.result = res;
		this.unparsed = up;
	}
}
