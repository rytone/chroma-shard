package rytone.chroma.command.parsing;

import rytone.chroma.command.parsing.exceptions.InvalidArgumentTypeException;
import rytone.chroma.command.parsing.exceptions.ParseException;

public interface ArgumentType {
	public ParseResult parse(String unparsed) throws ParseException;

	public String getString() throws InvalidArgumentTypeException;
	public int getInt() throws InvalidArgumentTypeException;
}
