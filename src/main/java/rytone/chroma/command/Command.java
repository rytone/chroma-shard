package rytone.chroma.command;

import rytone.chroma.command.parsing.ArgumentType;

import java.util.HashMap;

public interface Command {
	public String[] getAliases();
	public HashMap<String, ArgumentType> getArgumentDef();
	public String getNameLocale();
	public String getDescLocale();

	public void onExecute(ExecuteParams params);
}
