package rytone.chroma.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rytone.chroma.Chroma;
import rytone.chroma.command.parsing.CommandParser;
import rytone.chroma.command.parsing.ArgumentType;
import rytone.chroma.command.parsing.exceptions.ExhaustedException;
import rytone.chroma.command.parsing.exceptions.ParseException;
import rytone.chroma.locale.ServerLocale;
import rytone.chroma.reporting.DiscordReporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommandHandler {
	private HashMap<String, Command> commands;
	private ArrayList<Command> nonAliasedCommands;
	private Chroma chroma;
	private DiscordReporter errorReport;
	private Logger logger;

	public CommandHandler(Chroma c) {
		this.commands = new HashMap<>();
		this.nonAliasedCommands = new ArrayList<>();
		this.chroma = c;
		this.errorReport = new DiscordReporter();
		this.logger = LoggerFactory.getLogger("CommandHandler");
	}

	public void register(Command cmd) {
		logger.debug("Registering command " + cmd.getAliases()[0]);
		this.nonAliasedCommands.add(cmd);
		for (String alias : cmd.getAliases()) {
			this.commands.put(alias, cmd);
		}
	}

	public void handle(CommandParser parser, MessageReceivedEvent evt, ServerLocale locale) {
		String cmdname = parser.command();
		Command cmd = this.commands.get(cmdname);
		if (cmd != null) {
			try {
				HashMap<String, ArgumentType> finalArgs = new HashMap<>();
				for (Map.Entry<String, ArgumentType> entry : cmd.getArgumentDef().entrySet()) {
					parser = parser.argument(entry.getValue(), entry.getKey());
					finalArgs.put(entry.getKey(), parser.getArgument(entry.getKey()));
				}
				logger.debug("Executing command " + cmdname);
				cmd.onExecute(new ExecuteParams(finalArgs, this.chroma, this.errorReport, evt, locale));
			} catch (ExhaustedException e) {
				evt.getChannel().sendMessage("insufficient arguments (replace with interactive prompt later)").queue();
			} catch (ParseException e) {
				evt.getChannel().sendMessage("your arguments were formatted wrong. " + e.getLocalizedMessage()).queue();
			}
		}
	}

	public ArrayList<Command> getNonAliasedCommands() {
		return this.nonAliasedCommands;
	}
}
