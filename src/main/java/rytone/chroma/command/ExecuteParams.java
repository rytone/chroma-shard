package rytone.chroma.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import rytone.chroma.Chroma;
import rytone.chroma.command.parsing.ArgumentType;
import rytone.chroma.locale.ServerLocale;
import rytone.chroma.reporting.DiscordReporter;

import java.util.HashMap;

public class ExecuteParams {
	private HashMap<String, ArgumentType> args;
	private Chroma chroma;
	private MessageReceivedEvent evt;
	private DiscordReporter reporter;
	private ServerLocale locale;

	public ExecuteParams(HashMap<String, ArgumentType> a, Chroma c, DiscordReporter r, MessageReceivedEvent e, ServerLocale l) {
		this.args = a;
		this.chroma = c;
		this.evt = e;
		this.reporter = r;
		this.locale = l;
	}

	public HashMap<String, ArgumentType> getArguments() {
		return this.args;
	}

	public Chroma getChroma() {
		return this.chroma;
	}

	public MessageReceivedEvent getMessageEvent() {
		return evt;
	}

	public DiscordReporter getReporter() {
		return this.reporter;
	}

	public ServerLocale getLocale() { return this.locale; }
}
