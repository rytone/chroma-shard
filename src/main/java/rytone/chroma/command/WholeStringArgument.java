package rytone.chroma.command;

import rytone.chroma.command.parsing.ArgumentType;
import rytone.chroma.command.parsing.ParseResult;
import rytone.chroma.command.parsing.exceptions.InvalidArgumentTypeException;

public class WholeStringArgument implements ArgumentType {
	private String parsed = "";

	public ParseResult parse(String toParse) {
		this.parsed = toParse;
		return new ParseResult(this, "");
	}

	public String getString() {
		return parsed;
	}

	public int getInt() throws InvalidArgumentTypeException {
		throw new InvalidArgumentTypeException("WholeStringArgument cannot return an int");
	}
}
