package rytone.chroma.command;

import rytone.chroma.command.parsing.ArgumentType;
import rytone.chroma.command.parsing.ParseResult;
import rytone.chroma.command.parsing.exceptions.InvalidArgumentTypeException;
import rytone.chroma.command.parsing.exceptions.ParseException;

public class IntegerArgument implements ArgumentType {
	private int parsed;

	public ParseResult parse(String unparsed) throws ParseException {
		String rawint = unparsed.split("\\s+")[0];

		try {
			parsed = Integer.parseInt(rawint);
		} catch (NumberFormatException e) {
			throw new ParseException(e.getLocalizedMessage());
		}

		return new ParseResult(this, unparsed.substring(rawint.length(), unparsed.length()).trim());
	}

	public String getString() throws InvalidArgumentTypeException {
		throw new InvalidArgumentTypeException("IntegerArgument cannot return a String");
	}

	public int getInt() {
		return parsed;
	}
}
