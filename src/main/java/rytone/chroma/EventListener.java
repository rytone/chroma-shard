package rytone.chroma;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rytone.chroma.command.CommandHandler;
import rytone.chroma.command.parsing.CommandParser;
import rytone.chroma.command.parsing.exceptions.InvalidPrefixException;
import rytone.chroma.command.parsing.exceptions.UnmatchedPrefixException;
import rytone.chroma.commands.*;
import rytone.chroma.database.ServerSettingVerifier;
import rytone.chroma.locale.ServerLocale;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class EventListener extends ListenerAdapter {
	private final Logger logger = LoggerFactory.getLogger("EventListener");
	private CommandHandler cHandler;
	private Chroma chroma;
	private ServerSettingVerifier serverVerifier;
	private HashMap<String, ServerLocale> serverLocaleProvs;

	public EventListener(Chroma c) {
		this.chroma = c;
		this.cHandler = new CommandHandler(this.chroma);

		this.cHandler.register(new EchoCommand());
		this.cHandler.register(new RepeatEchoCommand());
		this.cHandler.register(new StatusCommand());
		this.cHandler.register(new LocaleTestCommand());
		this.cHandler.register(new CatCommand());

		this.cHandler.register(new HelpCommand(this.cHandler));

		this.serverLocaleProvs = new HashMap<>();
	}

	@Override
	public void onReady(ReadyEvent e) {
		this.logger.info("Loading phase 2");

		this.logger.debug("Starting StatusWorker...");
		StatusWorker s = new StatusWorker(e.getJDA());
		s.start();

		List<String> ids = e.getJDA().getGuilds().stream().map(g -> g.getId()).collect(Collectors.toList());
		this.serverVerifier = new ServerSettingVerifier(chroma.getDatabase(), ids.toArray(new String[ids.size()]));
		for (String id : ids) {
			serverLocaleProvs.put(id, new ServerLocale(id, chroma.getDatabase(), chroma.getLocale()));
		}

		this.logger.info("Verifying server settings...");
		this.serverVerifier.verifyLocale(chroma.getLocale());

		this.logger.info("Done loading!");
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent evt) {
		JDA client = evt.getJDA();

		User author = evt.getAuthor();
		Message message = evt.getMessage();

		if (!author.equals(client.getSelfUser())) {
			logger.trace("<" + message.getAuthor().getName() + "@" + message.getGuild().getName() + "> " + message.getContent());

			try {
				CommandParser p = new CommandParser(message.getRawContent()).prefix("//");
				cHandler.handle(p, evt, serverLocaleProvs.get(evt.getGuild().getId()));
			} catch (InvalidPrefixException e) {
				logger.warn("exception while parsing command prefix: ", e.getLocalizedMessage());
			} catch (UnmatchedPrefixException e) {

			}
		}
    }
}
