package rytone.chroma.reporting;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import rytone.chroma.locale.LocaleProvider;
import rytone.chroma.locale.ServerLocale;

import java.io.PrintWriter;
import java.io.StringWriter;

public class DiscordReporter {
	private boolean debug = false;

	public void setDebug(boolean newval) {
		this.debug = newval;
	}

	public void reportException(Exception e, ServerLocale lc, MessageReceivedEvent evt) {
		if (this.debug) {
			String msg = lc.getLocalizedString("exceptions.genericDebug");

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			msg = msg + "`" + sw.toString() + "`";
			evt.getChannel().sendMessage(msg).queue();
		} else {
			String msg = lc.getLocalizedString("exceptions.generic");
			evt.getChannel().sendMessage(msg).queue();
		}
	}
}
