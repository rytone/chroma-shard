package rytone.chroma;

import com.beust.jcommander.JCommander;
import com.moandjiezana.toml.Toml;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rytone.chroma.load.CLIOptions;
import rytone.chroma.load.Config;
import rytone.chroma.locale.LocaleProvider;

import javax.security.auth.login.LoginException;
import java.io.File;

public class Chroma {
	private Logger logger;
	private CLIOptions cliopts;
	private Config config;
	private LocaleProvider locale;
	private Connection database;
	private EventListener evl;

	public void start(String[] args) {
		this.logger = LoggerFactory.getLogger("Chroma");

		this.logger.info("Loading phase 1");

		this.logger.debug("Parsing CLI options...");
		this.cliopts = new CLIOptions();
		new JCommander(cliopts, args);

		this.logger.debug("Reading config..");
		this.config = new Toml().read(new File(this.cliopts.configPath)).to(Config.class);

		this.logger.debug("Reading locale...");
		this.locale = new LocaleProvider(cliopts.localeFolder);

		this.logger.debug("Connecting to database...");
		this.database = RethinkDB.r.connection().hostname("localhost").port(28015).connect();

		this.logger.debug("Creating event listener...");
		this.evl = new EventListener(this);

		this.logger.info("Starting JDA...");
		try {
			new JDABuilder(AccountType.BOT).setToken(this.config.token).addListener(evl).buildBlocking();
		} catch (LoginException | InterruptedException | RateLimitedException e) {
			e.printStackTrace();
		}
	}

	public CLIOptions getCLIOptions() {
		return this.cliopts;
	}

	public LocaleProvider getLocale() {
		return this.locale;
	}

	public Connection getDatabase() {
		return this.database;
	}


	public static void main(String[] args) {
		new Chroma().start(args);
	}
}
