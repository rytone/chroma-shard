package rytone.chroma.locale;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;

import java.util.HashMap;

public class ServerLocale {
	private LocaleProvider lcProv;
	private Connection dbConn;
	private String serverId;

	public ServerLocale(String sid, Connection db, LocaleProvider lc) {
		this.lcProv = lc;
		this.dbConn = db;
		this.serverId = sid;
	}

	public String getLocalizedString(String path) {
		HashMap<String, Object> ss = RethinkDB.r.db("chroma").table("server_settings").get(this.serverId).run(dbConn);
		String lang = (String)ss.get("locale");
		return lcProv.getLocalizedString(lang, path);
	}
}
