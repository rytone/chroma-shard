package rytone.chroma.locale;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.stream.Stream;

public class LocaleProvider {
	private HashMap<String, JSONObject> locales;
	private Logger logger;

	public LocaleProvider(String path) {
		logger = LoggerFactory.getLogger("LocaleProvider");
		this.locales = new HashMap<>();

		try(Stream<Path> files = Files.walk(Paths.get(path))) {
			files.forEach(file -> {
				if (Files.isRegularFile(file)) {
					try {
						StringBuilder jsonSB = new StringBuilder();
						Files.lines(file).forEachOrdered(jsonSB::append);
						JSONObject obj = new JSONObject(jsonSB.toString());
						locales.put(obj.getString("language"), obj);
						logger.info("Successfully loaded locale " + obj.getString("language"));
					} catch (Exception e) {
						logger.warn("Failed to load locale " + file.toString() + ": " + e.getLocalizedMessage());
					}
				}
			});
		} catch (IOException e) {
			logger.warn("Failed to load locales: " + e.getLocalizedMessage());
		}
	}

	public String getLocalizedString(String language, String path) {
		JSONObject obj = locales.get(language);
		String[] pathSegs = path.split("\\.");
		for (int i = 0; i < pathSegs.length; i++) {
			if (i == pathSegs.length-1) {
				return obj.getString(pathSegs[i]);
			}
			obj = obj.getJSONObject(pathSegs[i]);
		}
		return "";
	}

	public boolean languageExists(String lang) {
		return locales.get(lang) != null;
	}
}
