package rytone.chroma.database;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rytone.chroma.locale.LocaleProvider;

import java.util.HashMap;

public class ServerSettingVerifier {
	Connection db_conn;
	String[] serverIds;
	Logger logger;

	public ServerSettingVerifier(Connection db, String[] sds) {
		logger = LoggerFactory.getLogger("ServerSettingVerifier");
		this.db_conn = db;
		this.serverIds = sds;
	}

	private void initializeServerSettings(String sid) {
		logger.info("Initializing server settings for " + sid);
		RethinkDB.r.db("chroma").table("server_settings").insert(new ServerSettingDefaults(sid).toRethink()).run(db_conn);
	}

	public void verifyLocale(LocaleProvider lc) {
		ServerSettingDefaults defaults = new ServerSettingDefaults("");
		for (String sid : serverIds) {
			logger.debug("Verifying locale for " + sid);
			HashMap<String, Object> ss = RethinkDB.r.db("chroma").table("server_settings").get(sid).run(db_conn);
			if (ss == null) {
				initializeServerSettings(sid);
			} else {
				if (!lc.languageExists((String)ss.get("locale"))) {
					logger.warn("Invalid locale " + ss.get("locale") + ", resetting to default...");
					RethinkDB.r.db("chroma").table("server_settings").get(sid).update(RethinkDB.r.hashMap("locale", defaults.locale)).run(db_conn);
				}
			}
		}
	}
}
