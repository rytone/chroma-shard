package rytone.chroma.database;

import com.rethinkdb.RethinkDB;

public class ServerSettingDefaults {
	public String id = "";
	public String locale = "English";

	public ServerSettingDefaults(String sid) {
		this.id = sid;
	}

	public Object toRethink() {
		RethinkDB r = RethinkDB.r;
		return r.hashMap("id", this.id)
				.with("locale", this.locale);
	}
}
