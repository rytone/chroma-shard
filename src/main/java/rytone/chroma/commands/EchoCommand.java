package rytone.chroma.commands;

import rytone.chroma.command.Command;
import rytone.chroma.command.ExecuteParams;
import rytone.chroma.command.WholeStringArgument;
import rytone.chroma.command.parsing.ArgumentType;

import java.util.HashMap;

public class EchoCommand implements Command {
	public String[] getAliases() {
		return new String[]{"echo"};
	}
	public HashMap<String, ArgumentType> getArgumentDef() {
		HashMap<String, ArgumentType> arguments = new HashMap<>();
		arguments.put("content", new WholeStringArgument());
		return arguments;
	}
	public String getNameLocale() {
		return "commands.echo.name";
	}
	public String getDescLocale() {
		return "commands.echo.desc";
	}

	public void onExecute(ExecuteParams params) {
		try {
			params.getMessageEvent().getChannel().sendMessage(params.getArguments().get("content").getString()).queue();
		} catch (Exception e) {
			params.getReporter().reportException(e, params.getLocale(), params.getMessageEvent());
		}
	}
}
