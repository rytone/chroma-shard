package rytone.chroma.commands;

import rytone.chroma.command.Command;
import rytone.chroma.command.ExecuteParams;
import rytone.chroma.command.WholeStringArgument;
import rytone.chroma.command.parsing.ArgumentType;

import java.util.HashMap;

public class LocaleTestCommand implements Command {
	public String[] getAliases() {
		return new String[]{"localetest", "lc"};
	}
	public HashMap<String, ArgumentType> getArgumentDef() {
		HashMap<String, ArgumentType> arguments = new HashMap<>();
		arguments.put("path", new WholeStringArgument());
		return arguments;
	}
	public String getNameLocale() {
		return "commands.localetest.name";
	}
	public String getDescLocale() {
		return "commands.localetest.desc";
	}

	public void onExecute(ExecuteParams params) {
		try {
			String localePath = params.getArguments().get("path").getString();
			params.getMessageEvent().getChannel().sendMessage(params.getLocale().getLocalizedString(localePath)).queue();
		} catch (Exception e) {
			params.getReporter().reportException(e, params.getLocale(), params.getMessageEvent());
		}
	}
}
