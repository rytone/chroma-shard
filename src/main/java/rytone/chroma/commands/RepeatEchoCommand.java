package rytone.chroma.commands;

import rytone.chroma.command.Command;
import rytone.chroma.command.ExecuteParams;
import rytone.chroma.command.IntegerArgument;
import rytone.chroma.command.WholeStringArgument;
import rytone.chroma.command.parsing.ArgumentType;

import java.util.HashMap;

public class RepeatEchoCommand implements Command {
	public String[] getAliases() {
	return new String[]{"repeatedecho", "pleasedont"};
}
	public HashMap<String, ArgumentType> getArgumentDef() {
		HashMap<String, ArgumentType> arguments = new HashMap<>();
		arguments.put("amount", new IntegerArgument());
		arguments.put("content", new WholeStringArgument());
		return arguments;
	}
	public String getNameLocale() {
		return "commands.repecho.name";
	}
	public String getDescLocale() {
		return "commands.repecho.desc";
	}

	public void onExecute(ExecuteParams params) {
		try {
			int amnt = params.getArguments().get("amount").getInt();
			if (amnt > 10) {
				params.getMessageEvent().getChannel().sendMessage("please no more than 10 thanks").queue();
			} else {
				for (int i = 0; i < amnt; i++) {
					params.getMessageEvent().getChannel().sendMessage(params.getArguments().get("content").getString()).queue();
				}
			}
		} catch (Exception e) {
			params.getReporter().reportException(e, params.getLocale(), params.getMessageEvent());
		}
	}
}
