package rytone.chroma.commands;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import rytone.chroma.command.Command;
import rytone.chroma.command.ExecuteParams;
import rytone.chroma.command.parsing.ArgumentType;

import java.util.HashMap;

public class CatCommand implements Command {
	public String[] getAliases() {
		return new String[]{"cat"};
	}
	public HashMap<String, ArgumentType> getArgumentDef() {
		HashMap<String, ArgumentType> arguments = new HashMap<>();
		return arguments;
	}
	public String getNameLocale() {
		return "commands.cat.name";
	}
	public String getDescLocale() {
		return "commands.cat.desc";
	}

	public void onExecute(ExecuteParams params) {
		try {
			HttpResponse<JsonNode> resp = Unirest.get("http://random.cat/meow").asJson();
			String url = resp.getBody().getObject().getString("file");
			params.getMessageEvent().getChannel().sendMessage(url).queue();
		} catch (Exception e) {
			params.getReporter().reportException(e, params.getLocale(), params.getMessageEvent());
		}
	}
}
