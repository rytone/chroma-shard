package rytone.chroma.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import rytone.chroma.command.Command;
import rytone.chroma.command.CommandHandler;
import rytone.chroma.command.ExecuteParams;
import rytone.chroma.command.parsing.ArgumentType;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

public class HelpCommand implements Command {
	public String[] getAliases() {
		return new String[]{"help", "commands", "cmds"};
	}
	public HashMap<String, ArgumentType> getArgumentDef() {
		HashMap<String, ArgumentType> arguments = new HashMap<>();
		return arguments;
	}
	public String getNameLocale() {
		return "commands.help.name";
	}
	public String getDescLocale() {
		return "commands.help.desc";
	}

	private CommandHandler handler;

	public HelpCommand(CommandHandler h) {
		this.handler = h;
	}

	public void onExecute(ExecuteParams params) {
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle("Help", "https://www.rytone.tk/");
		embed.setColor(new Color(0x255ba1));
		for (Command cmd : handler.getNonAliasedCommands()) {
			String name = params.getLocale().getLocalizedString(cmd.getNameLocale());
			String desc = params.getLocale().getLocalizedString(cmd.getDescLocale());

			String[] ca = cmd.getAliases();
			StringBuilder aliases = new StringBuilder();
			aliases.append(params.getLocale().getLocalizedString("commands.help.aliasPrefix"));
			for (int i = 0; i < ca.length; i++) {
				aliases.append(ca[i]);
				if (i + 1 < ca.length) {
					aliases.append(", ");
				}
			}

			StringBuilder usage = new StringBuilder();
			usage.append(params.getLocale().getLocalizedString("commands.help.usagePrefix"));
			usage.append("`//");
			usage.append(cmd.getAliases()[0]);
			for (Map.Entry<String, ArgumentType> opt : cmd.getArgumentDef().entrySet()) {
				usage.append(" <" + opt.getKey() + ">");
			}
			usage.append("`");

			embed.addField(name, usage.toString() + "\n" + aliases.toString() + "\n" + desc, false);
		}
		params.getMessageEvent().getChannel().sendMessage(embed.build()).queue();
	}
}
