package rytone.chroma.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import rytone.chroma.command.Command;
import rytone.chroma.command.ExecuteParams;
import rytone.chroma.command.parsing.ArgumentType;

import java.awt.Color;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class StatusCommand implements Command {
	private Runtime rt;

	public StatusCommand() {
		rt = Runtime.getRuntime();
	}

	public String[] getAliases() {
		return new String[]{"status", "stats"};
	}
	public HashMap<String, ArgumentType> getArgumentDef() {
		HashMap<String, ArgumentType> arguments = new HashMap<>();
		return arguments;
	}
	public String getNameLocale() {
		return "commands.status.name";
	}
	public String getDescLocale() {
		return "commands.status.desc";
	}

	public void onExecute(ExecuteParams params) {
		long st = System.nanoTime();
		Message msg = params.getMessageEvent().getChannel().sendMessage("...").complete();
		long elapsed = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - st);

		long free = rt.freeMemory();

		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("Status", "https://www.rytone.tk/");
		eb.setColor(new Color(0x255ba1));
		eb.addField("Memory Usage", String.valueOf(free/1024/1024) + "MB free", false);
		eb.addField("OS", System.getProperty("os.name") + " (" + System.getProperty("os.arch") + ")", false);
		eb.addField("Ping", String.valueOf(elapsed) + "ms (this is NOT your connection!)", false);

		msg.editMessage(eb.build()).queue();
	}
}
