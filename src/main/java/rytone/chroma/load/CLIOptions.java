package rytone.chroma.load;

import com.beust.jcommander.*;

public class CLIOptions {
	@Parameter(names = { "-config" }, description = "config file path")
	public String configPath = "./config.toml";

	@Parameter(names = { "-localedir" }, description = "folder where locale is located")
	public String localeFolder = "./locale/locales/";
}
